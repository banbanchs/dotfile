# 这是我的Archlinux 的配置文件

-----

## [oh my zsh](https://github.com/robbyrussell/oh-my-zsh)
    sudo pacman -S zsh

var `curl`

    curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh

var `wget`

    wget --no-check-certificate https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh

-----

## infinality

    yaourt -S freetype2-infinality fontconfig-infinality

基于 [用infinality美化你的字体](http://forum.ubuntu.org.cn/viewtopic.php?p=2697071)

-----

## [xunlei-lixian](https://github.com/iambus/xunlei-lixian.git)

    git clone https://github.com/iambus/xunlei-lixian.git
    sudo pacman -S aria2

加入密码:

    git config password "your password"

-----

## LilyTerm

*感谢  [当 GNOME Terminal 3.8 不再支持透明背景时——LilyTerm](http://m13253.blogspot.com/2013/04/when-gnome-terminal-3.8-no-longer-supports-background-transparency.html)*

    sudo pacman -S lilyterm
    mv LilyTerm.config ~/.config/lilyterm/default.config

-----

## config

- mimeapps.list (default app)

        ~/.local/share/applications/mimeapps.list

- fcitx-config

        $ cp fcitx_config ~/.config/fcitx/config

- mimeapps.list

        $ cp mimeapps.list ~/.local/share/applications/mimeapps.list

- Preferences.sublime-settings

- qtcreator

        $ mkdir -p ~/.config/QtProject
        $ cp qtcreator ~/.config/QtProject/

-----

## etc

- monokai.xml (Qtcreator)

- app\_list.md (all applications list); `update method`:

        $ pacman -Q | awk '{print $1}' | sed 's/^/- /g' > app_list.md


- .xininrc

- .xprofile

- .gitconfig

- .dmrc
