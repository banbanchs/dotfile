#!/bin/sh

sudo tar -cjf /home/memory/Dropbox/Linux/etc-backup.tar.bz2 /etc
tar -cfv /home/memory/Dropbox/Linux/config-backup.tar.gz .config --exlude=.config/google-chrome
