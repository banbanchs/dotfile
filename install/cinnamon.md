## cinnamon

### Install

    $ sudo pacman -S cinnamon cinnamon-screensaver cinnamon-control-center
    $ sudo pacman -S nautilus # gnome file manager

*Starting Cinnamon manually*
>~/.xinitrc

> exec gnome-session-cinnamon

### theme

icon-theme

    $ sudo pacman -S faience-icon-theme

gtk-theme

    $ yaourt -S gtk-theme-numix

cursor

    $ yaourt -S lil-polar

-----

### Troubleshooting

QGtkStyle unable to detect the current theme

    $ sudo pacman -S libgnome-data

    $ gconftool-2 --set --type string /desktop/gnome/interface/icon_theme Faience

The cursor theme for Qt apps

    $ mkdir ~/.icons

    $ ln -s /usr/share/icons/lil-polar ~/.icons/default

Pressing power buttons suspend the system

    $ gsettings set org.gnome.settings-daemon.plugins.power button-power 'interactive'

Icons do not show on the desktop

    $ gsettings set org.gnome.desktop.background show-desktop-icons true

Volume level is not saved

    $ sudo pacman -S alsa-utils