## Atheros AR8161网卡驱动

    $ wget https://www.kernel.org/pub/linux/kernel/projects/backports/2013/03/28/compat-drivers-2013-03-28-5-u.tar.bz2
    $ tar xjf compat*
    $ cd compat*
    $ ./scripts/driver-select alx
    $ make
    $ sudo make install
    $ sudo modprobe alx

----

显卡驱动

    $ lspci | grep VGA

Intel

    # pacman -S xf86-video-intel intel-dri libva-intel-driver

NVIDIA

    # pacman -S nvidia libvdpau nvidia-utils

bumblebee & bbswitch

    # pacman -S bumblebee bbswitch
    # usermod -a -G bumblebee $USER(memory)
    # systemctl enable bumblebeed.service
