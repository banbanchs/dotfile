## INSTALL

分区

    # cfdisk

刷新分区表

    # partprobe /dev/sda

格式化分区

    # mkfs.ext4 /dev/sdax

挂载

    # mount /dev/sda8 /mnt
    # mkdir /mnt/home
    # mount /dev/sda9 /mnt/home

连接wifi

    # wifi-menu

修改源

    #  nano /etc/pacman.d/mirrorlist

>/etc/pacman.d/mirrorlist

>Server = http://mirrors.ustc.edu.cn/archlinux/$repo/os/$arch

安装基础

    # pacstrap /mnt base base-devel

安装Grub

     # pacstrap /mnt grub-bios

挂载脚本

    # genfstab -p /mnt >> /mnt/etc/fstab

chroot

    # arch-chroot /mnt

修改时区为+08上海

    # timedatectl set-timezone Asia/Shanghai

建立主机名

    # hostnamectl set-hostname [my-hostname]

编码

    # nano /etc/locale.gen
    //取消注释en_US.UTF-8
    # locale-gen

创建和启用镜像

    # mkinitcpio -p linux

安装grub到MBR

    # grub-install /dev/sda
    # grub-mkconf -o /boot/grub/grub.cfg

密码

    # passwd

安装网络工具(wifi)

    # pacman -S wireless_tools iw wpa_supplicant

Yaourt

添加Yaourt源至 /etc/pacman.conf:

>/etc/pacman.conf

>[archlinuxcn]
>Server = http://repo.archlinuxcn.org/$arch 

同步并安装：

    # pacman -Syu yaourt

字体

    # pacman -S ttf-dejavu
    # pacman -S wqy-zenhei
    # yaourt -S wqy-microhei

添加用户

    # useradd -m -g users -s /bin/bash memory
    # passwd memory

sudo

    # pacman -S sudo
    # visudo /etc/sudoers

>/etc/sudoers

>memory   ALL=(ALL) ALL

安装Xorg

    # pacman -S xorg-server xorg-xinit xorg-utils xorg-server-utils

退出chroot

    # exit

取消挂载

    # umount /mnt
    # reboot
